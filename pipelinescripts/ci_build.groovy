node {
   stage('Code Checkout') { // for display purposes
      git 'https://anoopsimon90@bitbucket.org/anoopsimon90/ci.git'
   }
   stage('Smoke') {
      echo 'Hello'
      bat '''cd "C:\\Users\\Anoop.Simon\\Documents\\visual studio 2015\\Projects\\QA.Automation.Framework\\packages\\NUnit.ConsoleRunner.3.7.0\\tools"
nunit3-console.exe "C:\\Users\\Anoop.Simon\\Documents\\visual studio 2015\\Projects\\QA.Automation.Framework\\UI\\bin\\Debug\\UI.dll"
'''
        nunit testResultsPattern:'TestResult.xml'
   }

   stage('API') {
     publishHTML (target: [
            reportDir: 'C:/Users/Anoop.Simon/.jenkins/workspace/E2E',
            reportFiles: 'TestResult.xml',
            reportName: "Smoke tests report"
            ])


   }
   stage('UI') {

   }
   stage('Results') {

   }
}
